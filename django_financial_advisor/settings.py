import os

import sentry_sdk
from decouple import config
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# All settings common to all environments
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
PROJECT_NAME = os.path.basename(PROJECT_ROOT)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config("SECRET_KEY", cast=str)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config("DEBUG", default=False, cast=bool)

# Master Data Defaults
MDAT_ROOT_CUSTOMER_ID = config("MDAT_ROOT_CUSTOMER_ID", default=1, cast=int)

SESSION_ENGINE = "user_sessions.backends.db"

SILENCED_SYSTEM_CHECKS = ["captcha.recaptcha_test_key_error", "admin.E410"]

SESSION_COOKIE_SECURE = not DEBUG

CSRF_COOKIE_SECURE = not DEBUG

SESSION_COOKIE_HTTPONLY = True

CSRF_COOKIE_HTTPONLY = True

SESSION_COOKIE_NAME = "__Secure-sessionid" if not DEBUG else "sessionid"

CSRF_COOKIE_NAME = "__Secure-csrftoken" if not DEBUG else "csrftoken"

ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = [
    "127.0.0.1",
]
CORS_ORIGIN_ALLOW_ALL = True

WATCHMAN_TOKENS = config("WATCHMAN_TOKENS", default=SECRET_KEY, cast=str)

APPEND_SLASH = False

TASTYPIE_ALLOW_MISSING_SLASH = True

# Application definition
INSTALLED_APPS = [
    # WhiteNoise - static file handling
    "whitenoise.runserver_nostatic",
    # CORS headers
    "corsheaders",
    # Debug Toolbar
    "debug_toolbar",
    # Django monitoring
    "watchman",
    # Minify,
    "django_minify_html",
    # multi-language
    "parler",
    # Django modules
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "user_sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # Django Sorcery - SQLAlchemy
    "django_sorcery",
    # Celery task
    "django_celery_results",
    "django_celery_beat",
    "flower",
    # API
    "tastypie",
    # own modules
    "django_financial_advisor_mdat.django_financial_advisor_mdat",
    "django_financial_advisor_strat_test.django_financial_advisor_strat_test",
    "django_financial_advisor_strats.django_financial_advisor_strats",
    "django_financial_advisor_scalable_capital.django_financial_advisor_scalable_capital",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.gzip.GZipMiddleware",
    "django_minify_html.middleware.MinifyHtmlMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django_sorcery.db.middleware.SQLAlchemyMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "user_sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "x_forwarded_for.middleware.XForwardedForMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

STATICFILES_STORAGE = "whitenoise.storage.CompressedStaticFilesStorage"

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

ROOT_URLCONF = "django_financial_advisor.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

# Celery
CELERY_RESULT_BACKEND = "django-db"
DJANGO_CELERY_BEAT_TZ_AWARE = False
CELERY_TASK_DEFAULT_QUEUE = "django_financial_advisor"

WSGI_APPLICATION = "django_financial_advisor.wsgi.application"

# Sentry
SENTRY_DSN = config("SENTRY_DSN", default="sentry_dsn", cast=str)
if not DEBUG and not SENTRY_DSN == "sentry_dsn":
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        send_default_pii=True,
        traces_sample_rate=0.1,
        integrations=[DjangoIntegration(), CeleryIntegration()],
    )

# Database
# https://docs.djangoproject.com/en/stable/ref/settings/#databases

# MAIN DATABASE
MAIN_DATABASE_NAME = config("MAIN_DATABASE_NAME", default="maindb", cast=str)
MAIN_DATABASE_USER = config("MAIN_DATABASE_USER", default="maindb", cast=str)
MAIN_DATABASE_PASSWD = config("MAIN_DATABASE_PASSWD", default="secret", cast=str)
MAIN_DATABASE_HOST = config("MAIN_DATABASE_HOST", default="127.0.0.1", cast=str)
MAIN_DATABASE_PORT = config("MAIN_DATABASE_PORT", default="3306", cast=str)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": MAIN_DATABASE_NAME,
        "USER": MAIN_DATABASE_USER,
        "PASSWORD": MAIN_DATABASE_PASSWD,
        "HOST": MAIN_DATABASE_HOST,
        "PORT": MAIN_DATABASE_PORT,
        "OPTIONS": {"init_command": "SET sql_mode='STRICT_TRANS_TABLES'"},
    },
}

# Password validation
# https://docs.djangoproject.com/en/stable/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = config("LANGUAGE_CODE", default="en", cast=str)

TIME_ZONE = config("TIME_ZONE", default="UTC", cast=str)

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")

# ========================================================================================
# PROJ - GENERAL SETTINGS
PROJ_GENERAL_ISIN = config("PROJ_GENERAL_ISIN", default="FB", cast=str)
PROJ_HTTP_UA = config(
    "PROJ_HTTP_UA",
    default="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36",
    cast=str,
)

# PROJ - SCALABLE CAPITAL
PROJ_SC_EMAIL = config("PROJ_SC_EMAIL", default="nobody@example.com", cast=str)
PROJ_SC_PASSWD = config("PROJ_SC_PASSWD", default="secret", cast=str)
PROJ_SC_PORTFOLIO_ID = config("PROJ_SC_PORTFOLIO_ID", default="abc123", cast=str)
